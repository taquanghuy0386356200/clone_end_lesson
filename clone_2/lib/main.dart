import 'package:clone_2/screens/home_page.dart';
import 'package:clone_2/theme/config_theme.dart';
import 'package:clone_2/theme/theme_custom.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter practice',
      theme: CustomTheme.lightMode,
      themeMode: currentTheme.currentTheme,
      darkTheme: CustomTheme.darkMode,
      home: HomePage(),
    );
  }
}
