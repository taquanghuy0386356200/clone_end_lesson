import 'dart:convert';

import 'package:clone_2/models/food.dart';
import 'package:http/http.dart' as http;

class RestfulApi {
  String _urlApi =
      'https://api.spoonacular.com/recipes/random?number=10&apiKey=9e4c4224a5394d0e81a77de12356c13f';
  // String _urlGetReceiptsById =
  //     'https://api.spoonacular.com/recipes/{id}/ingredientWidget.json';
  Future<List<Food>> getFoods() async {
    final response = await http.get(Uri.parse(_urlApi));
    if (response.statusCode == 200) {
      final List<Food> foods = [];
      final jsonData = jsonDecode(response.body) as Map<String, dynamic>; //map
      //json['recipes] as list
      if (jsonData.containsKey('recipes')) {
        final results = jsonData['recipes'] as List<dynamic>;
        for (Map<String, dynamic> item in results) {
          final info = Food.fromJson(item);
          foods.add(info);
        }
      } else {
        //nothing
      }
      return foods;
    } else {
      throw Exception('Failed to load foods');
    }
  }
}
