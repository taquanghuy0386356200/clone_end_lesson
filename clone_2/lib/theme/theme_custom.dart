import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'color.dart';

class CustomTheme extends ChangeNotifier {
  bool isDarkMode = false;

  ThemeMode get currentTheme => isDarkMode ? ThemeMode.dark : ThemeMode.light;

  void change() {
    isDarkMode = !isDarkMode;
    notifyListeners();
  }

  static ThemeData lightMode = ThemeData(
      appBarTheme: AppBarTheme(
    backgroundColor: MyColor.purplishBlue,
    brightness: Brightness.light,
  ));

  static ThemeData darkMode = ThemeData(
    appBarTheme: AppBarTheme(
      backgroundColor: MyColor.darkColor,
      brightness: Brightness.dark,
    ),
  );
}
