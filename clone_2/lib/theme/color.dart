import 'dart:ui';

class MyColor {
  static Color purplishBlue = const Color(0xff651fff);
  static Color darkColor = const Color(0xff191919);
}
