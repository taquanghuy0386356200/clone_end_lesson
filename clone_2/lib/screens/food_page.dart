import 'package:clone_2/models/food.dart';
import 'package:clone_2/responsitories/restful_api.dart';
import 'package:clone_2/screens/detailScreen/detail_page.dart';
import 'package:flutter/material.dart';

class FoodPage extends StatefulWidget {
  const FoodPage({Key? key}) : super(key: key);

  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  RestfulApi _api = RestfulApi();
  late Future<List<Food>> futureFoods;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        child: Center(
          child: FutureBuilder(
              future: _api.getFoods(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Food> foods = snapshot.data as List<Food>;
                  // log(foods.toString());
                  return ListView.builder(
                    itemCount: foods.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        child: Card(
                          child: foodItem(
                            image: foods[index].image,
                            title: foods[index].title,
                            time: foods[index].readyInMinutes,
                            summary: 'this is description about this food',
                            vegan: foods[index].vegan,
                            healthy: foods[index].veryHealthy,
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailPage(
                                food: foods[index],
                              ),
                            ),
                          );
                        },
                      );
                    },
                  );
                } else if (snapshot.hasError) {
                  return Text('has error');
                } else {
                  return CircularProgressIndicator();
                }
              }),
        ),
      ),
    );
  }

  Widget item(
      {required IconData icon,
      required String quantity,
      required Color color}) {
    return Column(
      children: [
        Icon(
          icon,
          color: color,
        ),
        Text(
          quantity,
          style: TextStyle(color: color),
        ),
      ],
    );
  }

  Widget foodItem(
      {required String image,
      required String title,
      required String summary,
      required int time,
      required bool vegan,
      required bool healthy}) {
    return Row(
      children: [
        Expanded(
          child: Image.network(image),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(summary),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    item(
                      icon: Icons.favorite,
                      quantity: healthy ? 100.toString() : 0.toString(),
                      color: healthy ? Colors.red : Colors.black,
                    ),
                    item(
                      icon: Icons.schedule,
                      quantity: time.toString(),
                      color: Colors.orangeAccent,
                    ),
                    item(
                      icon: Icons.eco,
                      quantity: vegan ? 'vegan' : 'not vegan',
                      color: vegan ? Colors.greenAccent : Colors.black,
                    )
                  ],
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
