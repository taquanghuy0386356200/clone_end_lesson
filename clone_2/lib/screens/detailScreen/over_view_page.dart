import 'package:clone_2/models/food.dart';
import 'package:flutter/material.dart';

class OverViewPage extends StatelessWidget {
  const OverViewPage({Key? key, required this.food}) : super(key: key);
  final Food food;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            child: Stack(
              children: [
                Image.network(
                  food.image,
                ),
                Positioned(
                  bottom: 25,
                  right: 16,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      item(
                          iconData: Icons.favorite,
                          quantity: food.veryHealthy ? '100' : '0'),
                      SizedBox(
                        width: 10,
                      ),
                      item(
                          iconData: Icons.schedule,
                          quantity: food.readyInMinutes.toString()),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Text(
                  food.title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    rowItems(
                      isTrue: food.vegan,
                      description: 'Vegan',
                    ),
                    rowItems(
                      isTrue: food.dairyFree,
                      description: 'Dairyt Free',
                    ),
                    rowItems(
                      isTrue: food.veryHealthy,
                      description: 'Healthy',
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    rowItems(
                      isTrue: food.vegetarian,
                      description: 'Vegetarian',
                    ),
                    rowItems(
                      isTrue: food.glutenFree,
                      description: 'GlutenFree Free',
                    ),
                    rowItems(
                      isTrue: food.cheap,
                      description: 'Cheap',
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Text('Description'),
                SizedBox(
                  height: 15,
                ),
                Text(food.summary),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget item({required IconData iconData, required String quantity}) {
    return Column(
      children: [Icon(iconData), Text(quantity)],
    );
  }

  Widget rowItems({required bool isTrue, required String description}) {
    return Row(
      children: [
        isTrue
            ? Icon(
                Icons.money,
                color: Colors.green,
              )
            : Icon(
                Icons.money,
                color: Colors.grey,
              ),
        isTrue
            ? Text(
                description,
                style: (TextStyle(
                  color: Colors.green,
                )),
              )
            : Text(
                description,
                style: (TextStyle(
                  color: Colors.grey,
                )),
              ),
      ],
    );
  }
}
