import 'package:clone_2/models/food.dart';
import 'package:flutter/material.dart';

class IngredientPage extends StatefulWidget {
  const IngredientPage({Key? key, required this.food}) : super(key: key);
  final Food food;
  @override
  _IngredientPageState createState() => _IngredientPageState();
}

class _IngredientPageState extends State<IngredientPage> {
  final String urlImage = 'https://spoonacular.com/cdn/ingredients_100x100/';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView.builder(
          itemCount: widget.food.ingredients.length,
          itemBuilder: (context, index) {
            return ingredientItem(
              image: widget.food.ingredients[index].image,
              consistency: widget.food.ingredients[index].consistency,
              name: widget.food.ingredients[index].name,
            );
          },
        ),
      ),
    );
  }

  Widget ingredientItem(
      {required String image,
      required String consistency,
      required String name}) {
    return Card(
      child: ListTile(
        leading: Image.network(urlImage + image),
        // leading: ConstrainedBox(
        //   constraints: BoxConstraints(
        //     maxHeight: 50,
        //     minHeight: 50,
        //     maxWidth: 50,
        //     minWidth: 50,
        //   ),
        //   child: Image.network(urlImage + image),
        // ),
        title: Text(name),
        subtitle: Text(consistency),
      ),
    );
  }
}
