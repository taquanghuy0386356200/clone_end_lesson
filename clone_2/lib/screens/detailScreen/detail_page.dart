import 'package:clone_2/models/food.dart';
import 'package:clone_2/screens/detailScreen/ingredient_page.dart';
import 'package:clone_2/screens/detailScreen/instruction_page.dart';
import 'package:clone_2/screens/detailScreen/over_view_page.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key, required this.food}) : super(key: key);
  final Food food;

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Details'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(10),
            child: Align(
              alignment: Alignment.center,
              child: TabBar(
                tabs: [
                  Text('Overview'),
                  Text('Ingredients'),
                  Text('Instructions'),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
          children: [
            Center(
              child: OverViewPage(food: widget.food),
            ),
            Center(
              child: IngredientPage(
                food: widget.food,
              ),
            ),
            Center(
              child: InstructionPage(
                food: widget.food,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
