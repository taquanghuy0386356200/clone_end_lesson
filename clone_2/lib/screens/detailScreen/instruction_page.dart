import 'package:clone_2/models/food.dart';
import 'package:flutter/material.dart';

class InstructionPage extends StatefulWidget {
  const InstructionPage({Key? key, required this.food}) : super(key: key);
  final Food food;
  @override
  _InstructionPageState createState() => _InstructionPageState();
}

class _InstructionPageState extends State<InstructionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView.builder(
          itemCount: widget.food.instructions.length,
          itemBuilder: (context, index) {
            return instructionItem(
              number: widget.food.instructions[index].step,
              step: widget.food.instructions[index].nameStep,
            );
          },
        ),
      ),
    );
  }

  Widget instructionItem({required int number, required String step}) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Row(
            children: [
              Icon(
                Icons.face_retouching_natural_sharp,
                color: Colors.green,
              ),
              Column(
                children: [
                  Text('Step ' + number.toString()),
                  Text(
                    'This is step ' + number.toString(),
                    style: TextStyle(color: Colors.green),
                  ),
                ],
              )
            ],
          ),
          Text(step),
        ],
      ),
    );
  }
}
