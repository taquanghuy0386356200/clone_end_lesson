import 'package:clone_2/models/ingredient.dart';
import 'package:clone_2/models/instruction.dart';

class Food {
  final int id;
  final String title;
  final String image;
  final String summary;
  final bool vegan;
  final bool veryHealthy;
  final int readyInMinutes;
  final bool cheap;
  final bool dairyFree;
  final bool glutenFree;
  final bool vegetarian;
  final List<Ingredient> ingredients;
  final List<Instruction> instructions;

  Food({
    required this.id,
    required this.title,
    required this.image,
    required this.summary,
    required this.vegan,
    required this.veryHealthy,
    required this.readyInMinutes,
    required this.cheap,
    required this.dairyFree,
    required this.glutenFree,
    required this.vegetarian,
    required this.ingredients,
    required this.instructions,
  });

  factory Food.fromJson(Map<String, dynamic> json) {
    int id = json['id'];
    String image = json['image'];
    String title = json['title'];
    String summary = json['summary'];
    bool vegan = json['vegan'];
    bool veryHealthy = json['veryHealthy'];
    int readyInMinutes = json['readyInMinutes'];
    bool cheap = json['cheap'];
    bool dairyFree = json['dairyFree'];
    bool glutenFree = json['glutenFree'];
    bool vegetarian = json['vegetarian'];
    List<Ingredient> ingredients = [];
    List<Instruction> instructions = [];
    final extendedIngredients = json['extendedIngredients'] as List<dynamic>;
    final analyzedInstructions = json['analyzedInstructions'] as List<dynamic>;
    //analyst ingredients
    for (Map<String, dynamic> ingredient in extendedIngredients) {
      ingredients.add(
        Ingredient(
            id: ingredient['id'],
            image: ingredient['image'],
            name: ingredient['name'],
            consistency: ingredient['consistency']),
      );
    }
    //analyst instruction
    for (Map<String, dynamic> intruction in analyzedInstructions) {
      for (Map<String, dynamic> step in intruction['steps'] as List<dynamic>) {
        instructions.add(
          Instruction(step: step['number'], nameStep: step['step']),
        );
      }
    }
    return Food(
      id: id,
      title: title,
      image: image,
      summary: summary,
      vegan: vegan,
      veryHealthy: veryHealthy,
      readyInMinutes: readyInMinutes,
      cheap: cheap,
      dairyFree: dairyFree,
      glutenFree: glutenFree,
      vegetarian: vegetarian,
      ingredients: ingredients,
      instructions: instructions,
    );
  }
}
