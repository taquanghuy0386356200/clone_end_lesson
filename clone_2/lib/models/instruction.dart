class Instruction {
  final int step;
  final String nameStep;

  Instruction({required this.step, required this.nameStep});
}
