class Ingredient {
  final int id;
  final String image;
  final String name;
  final String consistency;

  Ingredient(
      {required this.id,
      required this.image,
      required this.name,
      required this.consistency});
}
